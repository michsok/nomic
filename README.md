# Zasady

## Zasady konstytucyjne

1001. `konst` 

Wszyscy gracze muszą zawsze przestrzegać wszystkich zasad obowiązujących w danym momencie. Każda zasada posiada swój numer. Nie może być dwóch zasad o tym samym numerze pośród zasad obecnie obowiązujących i tych które kiedykolwiek wcześniej obowiązywały. Zasady są jednoznacznie identyfikowane po numerze.

1002. `konst` (O zasadach konstytucyjnych)
 
 Każda zasada zaczynająca się od słowa "`konst`" jest zasadą `konstytucyjną`. Wszystkie pozostałe zasady sa  zasadami `pozakonstytucyjnymi`.

1003. `konst` (O zmianach zasad)

Zmiana zasad jest jednym z poniższych: 
  - uchwaleniem, uchyleniem lub zmianą zasady `pozakonstytucyjnej`
  - transmutacją zasady `konstytucyjnej` w `pozakonstytucyjną` lub odwrotnie.


1004. `konst`  (O propozycjach zmian zasad)

Wszystkie propozycje zmian zasad powinny być przedstawione w postaci [Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/) (modyfikującego niniejszy dokument) przed głosowaniem. Wszystkie propozycje zmian zasad podlegają głosowaniu. Zasady zostają uchwalone wraz z chwilą zaakceptowania (merge) danego Merge Request do brancha `main` niniejszego repozytorium.

1006. `konst`

Zmiana zasad kończy się sukcesem, jeśli więcej głosów zostanie oddanych za przyjęciem zmiany zasad niż za odrzuceniem zmiany zasad. Wyjątkiem jest transmutacja, która kończy się sukcesem wtedy i tylko wtedy gdy wszystkie głosy zostaną jednogłośnie oddane za tym procesem.

1007. `konst` (O numeracji zasad)

 W przypadku gdy zmiana zasad dotyczy:
  - uchwalenia nowej zasady: nowa zasada otrzymuje numer będący najmniejszą liczbą naturalną, spełniającą założenia zasady 1001.
  - zmianą/transmutacją zasady: nie jest tworzona nowa zasada, więc numer zmienianej/transmutowanej zasady zostaje taki sam.


1008. `konst` (O wyższości zasad)

W przypadku konfliktu zasady `konstytucyjnej` z `pozakonstytucyjną`, zasada `konstytucyjna` ma wyższość nad zasadą `pozakonstytucyjną`. Co więcej, zasada `pozakonstytucyjna` w takim wypadku przestaje obowiązywać w całości.

W przypadku konfliktu dwóch zasad `konstytucyjnych` lub dwóch zasad `pozakonstytucyjnych`, wyższość ma zasada z mniejszym numerem.

1009. `konst`

Zawsze musi być przynajmniej jedna obowiązująca zasada `pozakonstytucyjna`.

1010.  `konst`

Cokolwiek nie jest zabronione lub regulowane przez zasadę, jest dozwolone i nieregulowane, z jedynym wyjątkiem zmiany zasad, która jest dozwolona tylko wtedy, gdy zasada lub zbiór zasad wyraźnie lub pośrednio ją dopuszcza. 

1015. `konst`

Każdy z graczy ma dokładnie jeden równoważny głos w głosowaniu nad zmianami zasad.

## Zasady pozakonstytucyjne

1005. 

Każdy z graczy może uczestniczyć w każdym głosowaniu nad każdą propozycją zmiany zasad o ile posiada głos. W przypadku gdy gracz nie odda swoich głosów w ciągu 48h od rozpoczęcia głosowania, każdy głos takiego gracza jest uznawany za nieważny.

1011. 

Gra składa się z tur. W kolejności alfabetycznej, każdy z graczy rozgrywa swoją turę. Podczas swojej tury gracz może zaproponować zmianę zasad lub spasować. W przypadku chęci zmiany zasad gracz ma 48 godzin od rozpoczecia swojej tury na rozpoczęcie głosowania. Nie rozpoczęcie głosowania w określonym czasie skutkuje końcem tury gracza. W przypadku gdy zaproponowana zmiana zostanie odrzucona lub przyjęta, gracz kończy turę.

1012. 

Każdy z graczy zaczyna z punktami równymi 0.

1013. 

Gdy zmiana zasad gracza zostanie zaakceptowana, zyskuje on 10 punktów.

1014. 

Gdy gracz uzyska łącznie 100 punktów, gra się natychmiast kończy, a on wygrywa.

1016. 

Na kanale #nomic zabrania się używania słownictwa ściśle związanego z kryptowalutami. Za użycie któregokolwiek ze słów:

krypto, kryptowaluta, Bitcoin, blockchain, DAO, smart contract, DeFi, token, NFT, ICO

gracz otrzymuje -1 punkt 

1017. 
Gracz ma możliwość przekazania dowolnej liczby swoich punktów innemu graczowi. Aby to zrobić musi użyć komendy "!give @{nazwa użytkownika} {liczba punktów}". Przekazywana liczba punktów musi być liczbą naturalną i nie może być większa niż liczba punktów posiadanych przez przekazującego gracza. Efekt przekazania punktów jest natychmiastowy.

1018.
Na początku tygodnia (poniedziałek 00:00 czasu polskiego) każdy gracz, który w poprzednim tygodniu polubił conajmniej 3 polecanki innych graczy, z poprzedniego tygodnia, dostaje 1 punkt. 

Gracz, który zdobył najwięcej polubień od innych graczy pod swoimi polecankami z poprzedniego tygodnia, otrzymuje 1 punkt (w przypadku remisu nikt nie otrzymuje dodatkowego punktu).

Polecanka — post polecający daną rzecz (np. dzieło kultury) na kanale 🎖│polecanki.

Polubienie polecanki— zostawienie reakcji "<:polecanka1:950012860648542238>" (<:polecanka1:950012860648542238>) pod polecanką.

1019.
Podczas głosowania gracz ma możliwość nabycia jednego lub więcej dodatkowych głosów, zwanych głosami nabytymi. Każdy głos nabyty kosztuje 20 punktów i może być wykorzystany jedynie w głosowaniu, podczas którego został nabyty. Nabycie głosu/głosów musi nastąpić przed wykorzystaniem przez gracza głosu gwarantowanego, o którym mówi zasada 1015. Gracz nabywa głosy używając komendy "!buyvotes {liczba głosów}". Jeśli gracz nie posiada wystarczającej liczby punktów, aby nabyć zadeklarowaną liczbę głosów, to użycie komendy nie przyniesie żadnego efektu. Gracz automatycznie wykorzystuje wszystkie swoje głosy nabyte w momencie oddania swojego głosu gwarantowanego, przy czym każdy głos nabyty jest jednorazowy, równoważny głosowi gwarantowanemu  oraz jednoznaczny z nim w kontekście poparcia zaproponowanej zmiany, bądź jej odrzucenia.

1020.
1 §  Niech **spór** oznacza  "brak porozumienia w kwestii interpretacji zasad lub inną sytuację sporną uniemożliwiającą dalszą rozgrywkę"

2 §  **Sąd Boży** może służyć do rozwiązywania wszystkich **sporów**, oprócz tego, czy dana kwestia faktycznie jest **sporem**.

3 § W przypadku zaistnienia **sporu**, każdy gracz może zarządzić **Sąd Boży** za cenę będącą liczbą naturalną i przynajmniej połową jego punktów. Gracz w takim wypadku od razu traci te punkty.

4 § W przypadku rozpoczęcia **Sądu Bożego**, wszyscy gracze muszą podzielić się na dwie strony konfliktu. Każda strona jest zobowiązana do przedstawienia swojego stanowiska wobec **sporu** w formie pisemnej oraz wytypowanie czempiona w ciągu 48 godzin od rozpoczęcia **Sądu Bożego**. Gdy obie strony przedstawią swoje stanowiska oraz czempionów, czempioni mają 48 godzin na stoczenie pojedynku w formie dowolnej gry na jaką się obaj zgodzą.  W przypadku braku porozumienia co do gry lub remisu w grze, czempioni losują między sobą wygranego.

5 §  Każda gra lub losowanie musi być przeprowadzone w sposób umożliwiający śledzenie jego przebiegu wszystkim chętnym graczom.

6 § Bezpośrednio po pojedynku, stanowisko wobec **sporu** strony zwycięzcy jest uznawane za poprawne i obwiązujące, a stanowisko przegranego za niepoprawne i nieobowiązujące (O ile stanowiska są różne. Jeśli są takie same, to są razem uznawane za poprawne i obowiązujące). 

7 § W przypadku, gdy gracz który rozpoczął **Sąd Boży** jest po stronie wygranej, strona przegrana jest zaobowiązana w ciągu 24 godzin po pojedynku wypłacić mu dwukrotność kwoty jaką zapłacił on za rozpoczęcie **Sądu Bożego** W przypadku gdy strona przegrana ma za mało punktów, musi przekazać wszystkie swoje punkty wcześniej wspomnianemu graczowi i otrzymuje on ponadto tyle punktów, by miał ich przynajmniej tyle samo co w chwili rozpoczęcia **Sądu Bożego**.

8 § W dowolnej chwili chempion może poddać się, automatycznie przegrywając i okrywając swoją stronę hańbą. 
